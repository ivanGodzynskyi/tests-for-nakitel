<?php
class Main {
    //This array contains of names of files for comparing with.
    private static $mainList;

    //  This method returns arrays of dirs in $result[0] and files $result[1] from dir $path.
    private static function f_read_dir($path)
    {
        if($path[strlen($path)-1] == '/') {
            return null;
        }

        $descr = @opendir($path);
        if ($descr) {
            chdir($path);
            $d[] = null;
            $f[] = null;
            while ($obj = readdir($descr)) {
                if (is_dir($obj)) {
                    if ($obj != '.' && $obj != '..') {
                        $d[] = $obj;
                    }
                }
                if (is_file($obj)) {
                    $f[] = $obj;
                }
            }
            closedir($descr);
        } else exit('Can not open the folder');
        $result[0] = $d;
        $result[1] = $f;
        return $result;
    }

    //The trigger of class that do all logic.
    public static function printFromFolderSimilarToListFiles($fileList, $startPath) {
        if(sizeof($fileList)==0) return;

        self::$mainList = $fileList;
        self::f_read_subdir($startPath);
    }

    //This method opens all subdirectories in $path.
    private static function f_read_subdir($path)
    {
        $result = self::f_read_dir($path);
        if(sizeof($result[0])>0) {
            foreach ($result[0] as $dir) {
                self::f_read_subdir($path . '/' . $dir);
            }
        }
        if(sizeof($result[1])>0) {
            foreach ($result[1] as $file) {
                if (self::f_compareWithMainlist($file)) {
                    echo str_replace("/", "\\", $path) . '\\' . $file . '<br>';
                }
            }
        }
    }

    //Compare $file with all names from array $mainList if similar persent >= 90 return true else return false.
    private static function f_compareWithMainlist($file) {
        if(strlen($file)==0) return false;

        foreach (self::$mainList as $f) {
            similar_text($file,$f,$percent);
            if($percent>=90) {
                return true;
            }
        }
        return false;
    }
}
?>