<HTML>
<title>
    Test #1
</title>
<body>
<h3>
    Совершенно очевидно, что условие задачи содержит неточность. <br><br>
    1. Дан каталог с подкаталогами, содержащими файлы с разными именами.
    Также имеется файл main_list_name.txt с именами файлов.
    Требуется вывести имена и пути к файлам в каталоге, имена которых отличаются от списка с
    main_list_name.txt на 10% и более. <br><br>
    Если в списке имеется два имени, которые полностью отличаются, значит каждое имя
    будет отличаться от одного из них более чем на 10%. Так что это условие не совсем понятно. <br><br>
    Задача решалась при условии, что имена файлов отличаются на 10% и МЕНЕЕ.<br><br>
    Если это не опечатка, могу поправить код за 5 секунд.
</h3>
    <form action="<?php $_SERVER['SCRIPT_NAME']; ?>" method="POST">
        Path of the main folder contains "main_dir" and file "main_list_name.txt" (e.g. d:\php\Nakitel\):
        <input type="text" name="pathFolder" title="Enter location of main_list_name.txt"/>
        <input type="submit"/>
    </form>
</body>
<?php
header('Content-Type: text/html; charset=utf-8');
require_once('test_nakitel_script.php');
if (isset($_POST['pathFolder'])) {
    $pathFolder = str_replace('\\','/',$_POST['pathFolder']).'main_dir';
    $pathFile = str_replace('\\' ,'/', $_POST['pathFolder']).'main_list_name.txt';
} else {
    exit(1);
}
//Get files to array from main_list_name.txt
$string = @file_get_contents($pathFile);
if(!$string) {
    exit('Error reading data!');
}
$mainlist = explode(',', $string);
//Get files from main_dir, compare and print them
Main::printFromFolderSimilarToListFiles($mainlist,$pathFolder);
?>
</HTML>